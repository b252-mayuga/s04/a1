-- a
SELECT * FROM artists WHERE name LIKE "%d%";

-- b
SELECT * FROM songs WHERE length < 230;

-- c
SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- d
SELECT albums.* FROM albums
	JOIN artists ON albums.artist_id = artists.id WHERE albums.album_title LIKE "%a%";

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";

-- e
SELECT * FROM albums ORDER BY album_title DESC;

-- f
SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;